package com.example.firebaseexample.ui.main;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.firebaseexample.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainFragment extends Fragment {

    private SharedViewModel mViewModel;
    DatabaseReference uid;
    DatabaseReference users;
    DatabaseReference quedadas;
    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        DatabaseReference base = FirebaseDatabase.getInstance("https://carlesexample-f2c6f-default-rtdb.europe-west1.firebasedatabase.app/").getReference();
        DatabaseReference users = base.child("users");

        mViewModel = new ViewModelProvider(requireActivity()).get(SharedViewModel.class);
        mViewModel.getUser().observe(getViewLifecycleOwner(), user -> {
            uid = users.child(user.getUid());
            System.out.println(uid+"MF-GETUSER()");
            quedadas = uid.child("Quedadas");
        });
        System.out.println("MA-onCreateView");

        return inflater.inflate(R.layout.main_fragment, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("MA-onCreate");


    }



    @Override
    public void onStart() {
        super.onStart();
        System.out.println("MA-onStart");

    }

}